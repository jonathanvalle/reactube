import React, {useState} from 'react';
import Container from 'react-bootstrap/Container';
import Video from './components/Video';
import MyNav from './components/Mynav';
import DetailedVideo from "./components/DetailedVideo";
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Channel from "./components/Channel";
import DetailedChannel from "./components/DetailedChannel";

const App = () => {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, selectVideo] = useState({});
  const [currentValue, majValeur] = useState("channel");


  return (
    <Container className="p-3">
        <Router>
            <MyNav onResults={setVideos} currentValue={currentValue} majValeur={majValeur}/>

            <Switch>
                <Route path="/videos/:videoId">
                    <DetailedVideo id={selectedVideo.id}
                                   snippet={selectedVideo.snippet} player={selectedVideo.player}
                                   statistics={selectedVideo.statistics} />
                </Route>
                <Route path="/channels/:channelId">
                    <DetailedChannel id={selectedVideo.id}
                                   snippet={selectedVideo.snippet} statistics={selectedVideo.statistics}/>
                </Route>
                <Route path="/search/:search">
                    <>
                        {
                                videos.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                if(v.id.videoId!=null){
                                    return (<Video
                                        key={v.id.videoId}
                                        videoId={v.id.videoId}
                                        thumbnail={thumbnails.high.url}
                                        description={description}
                                        channelTitle={channelTitle}
                                        publishTime={publishTime}
                                        title={title}
                                        selectVideo={selectVideo}/>)
                                }else{
                                    return (<Channel
                                        key={v.id.channelId}
                                        channelId={v.id.channelId}
                                        thumbnail={thumbnails.high.url}
                                        description={description}
                                        publishAt={v.publishAt}
                                        title={title}
                                        selectVideo={selectVideo}
                                    />);
                                }
                                })

                        }
                    </>
                </Route>
                <Route path="/">
                    Merci d'effectuer une recherche...
                </Route>
            </Switch>
        </Router>
    </Container>
);
};


export default App;



