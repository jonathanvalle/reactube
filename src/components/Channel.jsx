import {Row,Col,Image, Card} from 'react-bootstrap';
import api from "../lib/api";
import {useHistory} from "react-router-dom";
import React from "react";

const Channel = (props) =>{

    const {selectVideo, channelId, thumbnail, description,publishAt, title} = props;
    const history = useHistory();

    const search = async () => {
        const resp = await api.get('/channels',
            { params: {
                    id: channelId,
                    maxWidth: 1080,
                    maxHeight: 720,
                    part: 'snippet,statistics',
                }}
        );
        console.log('Received', resp.data.items);
        selectVideo(resp.data.items[0]);
        history.push(`/channels/${channelId}`);
    };

    return (

        <Card>
            <Card.Body>
                <Row>
                    <Col md={4} style={{cursor: 'pointer'}}>
                        <Image src={thumbnail} fluid onClick={search}/>
                    </Col>
                    <Col>
                        <Card.Title onClick={search} style={{cursor: 'pointer'}}>{title}</Card.Title>
                        <Card.Text>
                            {description}
                        </Card.Text>
                        <Card.Text className="mb-1 text-muted">
                            {publishAt}
                        </Card.Text>
                    </Col>


                </Row>
            </Card.Body>
        </Card>
    )
}
export default Channel;
