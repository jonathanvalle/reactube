import Navbar from "react-bootstrap/Navbar";
import { Nav } from "react-bootstrap";
import SearchBar from './SearchBar';
const MyNav = ({ onResults, currentValue, majValeur }) => {

  return (<Navbar bg="dark" variant="dark">
    <Navbar.Brand href="/">Reactube</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link onClick={() => majValeur("video")} >Videos</Nav.Link>
      <Nav.Link onClick={() => majValeur("channel")} >Channels</Nav.Link>
    </Nav>
    <SearchBar onResults={onResults} currentValue={currentValue}/>
  </Navbar>);
}
export default MyNav;
