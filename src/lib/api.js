import axios from 'axios'
const key = 'AIzaSyA2E6iVbuIDYk6IhTbjhN1WLNfuskaoQ9E'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})
export default fetcher;
